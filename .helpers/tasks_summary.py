#!/usr/bin/env python3
#
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
#
# Script to print simple statistics about created tasks

import os
import yaml

from operator import itemgetter


def load_tasks_data(path='.'):
    """
    Loads YAML files from task and returns dict with all of them.
    """
    task_dirs = [i for i in os.scandir(path) if i.is_dir() and not i.name.startswith('.')]
    tasks_data = dict()

    # Load tasks data
    for task_dir in task_dirs:
        task_yaml_file = os.path.join(task_dir.path, 'task.yml')
        try:
            with open(task_yaml_file) as f:
                tasks_data[task_dir.name] = yaml.load(f)
        except:
            print("ERROR: can not open '{}'".format(task_yaml_file))
            continue
    
    return tasks_data


def count_tasks_per_language(tasks_data):
    """
    Counts number of tasks per language and returns dict with name of language and [count, list of tasks].
    """
    count = dict()
    for name, task_data in tasks_data.items():
        task_language = task_data['language'].upper()
        try:
            count[task_language][0] += 1
            count[task_language][1].append(name)
        except KeyError:
            count[task_language] = [1, [name]]
    return count


def main(): 
    tasks_data = load_tasks_data()
    count_per_language = count_tasks_per_language(tasks_data)

    print("\nTotal tasks: {}\n".format(len(tasks_data)))

    print("Tasks per language:\n{}".format('-' * 19))
    for language, count in sorted(count_per_language.items(), key=itemgetter(1), reverse=True):
        print("{} - {} - {}".format(count[0], language, sorted(count[1])))


if __name__ == '__main__':
    main()

