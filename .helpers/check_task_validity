#!/bin/perl

use strict;
use Data::Dumper;
use YAML::Syck;
use File::stat qw(:FIELDS);

my $opt_verbose = 1;

# Ohh, I know, this is sad story :(
my @subdirs=`find -maxdepth 1 -type d -and -not -name '.*'`;


sub dbg
{
    my ($str) = @_;
    print STDERR " ~ $str\n" if ($opt_verbose);
}


sub err
{
    my ($str) = @_;
    print STDERR "ERROR: $str\n";
}


sub check_subitems
{
    my ($config) = @_;

    my %allowed_hash = map { $_ => 1 } (
        'check_exit_status',
        'limits',
    );

    my %allowed_array = map { $_ => 1 } (
        'check_files',
        'setup_commands',
        'hints',
        'dependencies',
    );


    for my $key (keys %{$config}) {
        if (ref $config->{$key} eq ref {}
            && not exists($allowed_hash{$key})) {
            err "the '$key' in '$config->{name}' task is hash";
            return 0;
        }
        if (ref $config->{$key} eq ref []
            && not exists($allowed_array{$key})) {
            err "the '$key' in '$config->{name}' task is array";
            return 0;
        }
    }

    return 1;
}


sub check_max_points
{
    my ($config) = @_;
    my $max = $config->{max_score};
    my $hintref = $config->{hints};

    # not defined or not an array
    return 1 if ref $hintref ne ref [];

    my $hintcount = scalar @$hintref;

    if ($max <= $hintcount) {
        err "'$config->{name}' has max score <= number of hints";
        return 0;
    }

    return 1;
}


sub check_script
{
    my ($config) = @_;

    if (defined $config->{check_script}) {
        my $fullpath = './' . $config->{dir_name} . "/check/" . $config->{check_script};
        if (not -f $fullpath) {
            err "missing $fullpath check script";
            return 0;
        }
        stat($fullpath) or die "can't stat $fullpath";
        if (not $st_mode & 0100) {
            err "check script '$fullpath' is not executable '$st_mode'";
            return 0;
        }
    }
    else {
        err "missing check_script";
        return 0;
    }

    return 1;
}


sub check_sources_dir
{
    my ($config) = @_;
    my $fullpath = './' . $config->{dir_name} . "/source";
    if (-d $fullpath) {
        return 1;
    }

    err "missing source/ directory";
    return 0;
}


our $methods = {
    'subitems'  => \&check_subitems,
    'maxpoints' => \&check_max_points,
    'check_script' => \&check_script,
    'sources_dir' => \&check_sources_dir,
};

sub check_all
{
    my ($config) = @_;

    my $rv = 1;

    for my $key (keys %{$methods}) {
        $rv = 0 if $methods->{$key}($config) == 0;
        dbg " ... $key";
    }

    return $rv;
}


my $i = 1;
my $exit_val = 0;
for my $task_dir (@subdirs) {
    chomp $task_dir;

    $task_dir =~ s/^\.\///;

    my $yaml_file = "$task_dir/task.yml";

    dbg "task: $i, yaml file: $yaml_file";

    open my $fd, '<', $yaml_file
        or die "can't open yaml file '$yaml_file'";

    my $config = YAML::Syck::LoadFile($fd);
    $config->{dir_name} = $task_dir;
    $i++;

    $exit_val = 1 if check_all($config) == 0;
}

$exit_val ? print STDERR "Fail.\n" : print STDERR "Success.\n";

exit $exit_val;
