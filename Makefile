helpers = .helpers

NAME = bughunting-tasks
VERSION = $(shell awk '/^Version:/ {print $$2}' bughunting-tasks.spec)
RELEASE = $(shell awk '/^Release:/ {print $$2}' bughunting-tasks.spec)
VERSIONED_NAME = $(NAME)-$(VERSION)

RPM_BUILD_DIR=.rpm-build-dir

DESTDIR=

RPMBUILD_DEFINES = --define "_sourcedir `pwd`" --define "_srcrpmdir `pwd`/$(RPM_BUILD_DIR)" --define "_rpmdir `pwd`/$(RPM_BUILD_DIR)" --define "_specdir `pwd`/$(RPM_BUILD_DIR)"

archive:
	git archive --format=tar --prefix $(VERSIONED_NAME)/ HEAD | bzip2 > $(VERSIONED_NAME).tar.bz2

install:
	install -d -m 0755 $(DESTDIR)/var/lib/bughunting/tasks || exit 1 ;	\
	for i in `find -maxdepth 1 -type d -and -not -name '.*'`;		\
	do									\
	  cp -a "$$i" $(DESTDIR)/var/lib/bughunting/tasks ;			\
	  echo $$i ;								\
	done

package_requirements = perl-YAML-Syck perl-Data-Dumper

dependencies-pkg-deplist check-pkg-deplist:
	@rc=: ; \
	for pkg in $(package_requirements); do \
	    rpm -q $$pkg >/dev/null || { \
	        rc=false; \
	        echo "Error: missing package $$pkg" ; \
	    } ; \
	done ; $$rc


dependencies.txt: dependencies-pkg-deplist
	$(helpers)/generate_deplist > dependencies.txt

srpm: archive dependencies.txt
	rm -rf $(RPM_BUILD_DIR)
	mkdir $(RPM_BUILD_DIR)
	rpmbuild $(RPMBUILD_DEFINES) -bs bughunting-tasks.spec

rpms: srpm
	rpmbuild $(RPMBUILD_DEFINES) --rebuild $(RPM_BUILD_DIR)/*.src.rpm

clean:
	rm -rf $(RPM_BUILD_DIR)
	rm -f *.tar.bz2

check: dependencies-pkg-deplist
	$(helpers)/check_task_validity
